# Job Log Url Demo

This repo was created to demonstrate Gitlab Issue [452203](https://gitlab.com/gitlab-org/gitlab/-/issues/452203)

It consists of a simple script which echoes the four lines below:
```
blah blah [https://example.invalid/foo] blah blah
blah blah [blah https://example.invalid/foo] blah blah
blah blah [https://example.invalid/foo]blah blah
blah blah [blah https://example.invalid/foo]blah blah
blah blah [https://[2001:DB8::]/foo]
```

Fof the first four, the URL that the job log screen should link to is `https://example.invalid/foo`, however it incorrectly includes the trailing `]` and any text after that until the next space.

For the last one, the URL that the job log screen should link to is `https://[2001:DB8::]/foo` (the first `]` _is_ part of the URL) however it incorrectly includes the trailing `]` too.
